import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/apartment/:id',
    name: 'apartment',
    component: () => import('../views/Apartment.vue')
  },
  {
    path: '/apartment-list',
    name: 'apartmentList',
    component: () => import('../views/ApartmentList.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
